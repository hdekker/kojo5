<?php

/**
 * @file
 * Display Suite Responsive Two Column configuration.
 */

function ds_responsive_two_column() {
  return array(
    'label' => t('Responsive Two Column'),
    'regions' => array(
      'header' => t('Header'),
      'left' => t('Left'),
      'right' => t('Right'),
      'footer' => t('Footer'),
    ),
    // Uncomment if you want to include a CSS file for this layout (responsive_two_column.css)
    'css' => TRUE,
    // Uncomment if you want to include a preview for this layout (responsive_two_column.png)
    // 'image' => TRUE,
  );
}
